from contextlib import contextmanager

from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from conf import Config


@contextmanager
def create_session(*args, **kwargs):
    """
    Context manager with fe database session to use out of flask application context
    Usage with models: session.query(model).(needed_action)
    Models should be imported from db.fe.models.pss
    """
    engine = create_engine(Config().db_uri, *args, **kwargs)
    session = Session(engine)
    yield session
    session.close()
