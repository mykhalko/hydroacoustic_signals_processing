from sqlalchemy import Column, Integer, SMALLINT
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Pressure(Base):
    __tablename__ = "pressure"

    id = Column(Integer, primary_key=True)
    vx = Column(SMALLINT, nullable=False)
    vy = Column(SMALLINT, nullable=False)
    vz = Column(SMALLINT, nullable=False)
    p = Column(SMALLINT, nullable=False)


class ResearchObjectInfo(Base):
    __tablename__ = "research_object_info"

    id = Column(Integer, primary_key=True)
    x1 = Column(SMALLINT, nullable=False)
    x2 = Column(SMALLINT, nullable=False)
    y1 = Column(SMALLINT, nullable=False)
    y2 = Column(SMALLINT, nullable=False)
    depth = Column(SMALLINT, nullable=False)
    velocity = Column(SMALLINT, nullable=False)

