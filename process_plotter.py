

def plotter(file_id: int, out=None):
    import sys
    from data_utils import load_by_id
    from spectrum import sampling, filter_start
    import numpy as np
    from matplotlib import pyplot
    origin_stdout = sys.stdout
    if out:
        stream = open(out, "w")
        sys.stdout = stream
    ds = load_by_id(file_id)
    s_y, s_x, plot = pyplot.magnitude_spectrum(ds.p, Fs=2048)
    pyplot.close()
    s_y = filter_start(s_y)
    sampled_values = list(sampling(np.column_stack((s_x, s_y))))
    pyplot.bar(range(256), sampled_values)
    pyplot.plot(range(256), [sum(sampled_values) / len(sampled_values)] * 256, c="r")
    pyplot.show()
    sys.stdout.close()
    sys.stdout = origin_stdout
