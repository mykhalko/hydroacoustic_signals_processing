from PyQt5.Qt import QApplication, QMainWindow
import sys

from src.ui.initial_window import InitialAppScreenWidget


def main():
    app = QApplication(sys.argv)
    window = InitialAppScreenWidget()
    window.show()
    app.exec_()


if __name__ == "__main__":
    main()
