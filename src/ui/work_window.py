from PyQt5.QtWidgets import (
    QWidget,
    QListWidget,
    QHBoxLayout,
    QLineEdit,
    QStackedLayout,
    QVBoxLayout,
    QRadioButton,
    QButtonGroup,
    QCheckBox,
    QLabel,
    QPushButton
)

from core.listings import AbstractListing, VolumeListing, DatabaseListing
from core.plotter import plot


class SignalsListing(QListWidget):
    _listing: AbstractListing = None

    def __init__(self, *args, **kwargs):
        self._listing = kwargs.pop("listing")
        super().__init__(*args, **kwargs)
        self.update_listing("")

    def update_listing(self, filter_by: str):
        self.clear()
        self.addItems(self._listing.list_items(filter_by))


class WorkWindow(QWidget):
    _listing_folder: SignalsListing = None
    _listing_db: SignalsListing = None
    _input_search: QLineEdit = None

    _listing_stacked_layout: QStackedLayout = None

    _radio_button_folder: QRadioButton = None
    _radio_button_db: QRadioButton = None
    _radio_button_group: QButtonGroup = None

    _radio_button_oscillogram: QRadioButton = None
    _radio_button_spectrum: QRadioButton = None
    _radio_button_graph_type: QButtonGroup = None

    _checkbox_dimension_p: QCheckBox = None
    _checkbox_dimension_vx: QCheckBox = None
    _checkbox_dimension_vy: QCheckBox = None
    _checkbox_dimension_vz: QCheckBox = None

    _graph_type_stacked_layout: QStackedLayout = None

    _oscillogram_control_widget: QWidget = None
    _spectrum_control_widget: QWidget = None

    _radio_spectrum_filter_limit_avg: QRadioButton = None
    _radio_spectrum_filter_limit_stdev: QRadioButton = None
    _radio_spectrum_filter_limit_2stdev: QRadioButton = None
    _radio_spectrum_filter_limit_3stdev: QRadioButton = None
    _radio_spectrum_filter_limit_disabled: QRadioButton = None
    _radio_spectrum_filter_group_limit: QButtonGroup = None

    _button_draw: QPushButton = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.init_components()

    def init_components(self):

        main_layout = QHBoxLayout()

        listings_panel_layout = QVBoxLayout()
        self._listings_stacked_layout = QStackedLayout()

        self._input_search = QLineEdit()
        self._radio_button_folder = QRadioButton("Folder")
        self._radio_button_folder.setChecked(True)
        self._radio_button_db = QRadioButton("Database")
        self._radio_button_group = QButtonGroup()
        self._radio_button_group.addButton(self._radio_button_folder)
        self._radio_button_group.addButton(self._radio_button_db)
        self._radio_button_group.buttonClicked.connect(self.change_listing)
        self._listing_folder = SignalsListing(listing=VolumeListing())
        self._listing_db = SignalsListing(listing=DatabaseListing())
        self._listings_stacked_layout.addWidget(self._listing_folder)
        self._listings_stacked_layout.addWidget(self._listing_db)
        self._listings_stacked_layout.setCurrentIndex(0)
        listings_panel_layout.addWidget(self._radio_button_folder)
        listings_panel_layout.addWidget(self._radio_button_db)
        listings_panel_layout.addWidget(self._input_search)
        listings_panel_layout.addLayout(self._listings_stacked_layout)

        main_layout.addLayout(listings_panel_layout)

        self._input_search.textChanged.connect(self.update_listings)

        draw_parameters_layout = QVBoxLayout()

        dimensions_layout = QHBoxLayout()
        self._checkbox_dimension_p = QCheckBox("P")
        self._checkbox_dimension_p.setStyleSheet("color: black")
        self._checkbox_dimension_vx = QCheckBox("Vx")
        self._checkbox_dimension_vx.setStyleSheet("color: red")
        self._checkbox_dimension_vy = QCheckBox("Vy")
        self._checkbox_dimension_vy.setStyleSheet("color: green")
        self._checkbox_dimension_vz = QCheckBox("Vz")
        self._checkbox_dimension_vz.setStyleSheet("color: blue")
        dimensions_layout.addWidget(self._checkbox_dimension_p)
        dimensions_layout.addWidget(self._checkbox_dimension_vx)
        dimensions_layout.addWidget(self._checkbox_dimension_vy)
        dimensions_layout.addWidget(self._checkbox_dimension_vz)
        draw_parameters_layout.addLayout(dimensions_layout)

        graph_type_layout = QHBoxLayout()
        self._radio_button_oscillogram = QRadioButton("oscillogram")
        self._radio_button_oscillogram.setChecked(True)
        self._radio_button_spectrum = QRadioButton("spectrum")
        self._radio_button_graph_type = QButtonGroup()
        self._radio_button_graph_type.buttonClicked.connect(self.change_graph_type)
        self._radio_button_graph_type.addButton(self._radio_button_oscillogram)
        self._radio_button_graph_type.addButton(self._radio_button_spectrum)
        graph_type_layout.addWidget(self._radio_button_oscillogram)
        graph_type_layout.addWidget(self._radio_button_spectrum)
        draw_parameters_layout.addLayout(graph_type_layout)

        self._graph_type_stacked_layout = QStackedLayout()

        self._oscillogram_control_widget = QWidget()
        oscillogram_layout = QVBoxLayout()

        self._oscillogram_control_start_input = QLineEdit()
        self._oscillogram_control_end_input = QLineEdit()
        self._oscillogram_control_start_label = QLabel("start")
        self._oscillogram_control_end_label = QLabel("end")
        start_layout = QHBoxLayout()
        start_layout.addWidget(self._oscillogram_control_start_label)
        start_layout.addWidget(self._oscillogram_control_start_input)
        oscillogram_layout.addLayout(start_layout)
        end_layout = QHBoxLayout()
        end_layout.addWidget(self._oscillogram_control_end_label)
        end_layout.addWidget(self._oscillogram_control_end_input)
        oscillogram_layout.addLayout(end_layout)
        self._oscillogram_control_widget.setLayout(oscillogram_layout)
        self._graph_type_stacked_layout.addWidget(self._oscillogram_control_widget)
        self._graph_type_stacked_layout.setCurrentIndex(0)


        self._spectrum_control_widget = QWidget()
        spectrum_layout = QVBoxLayout()
        self._spectrum_control_widget.setLayout(spectrum_layout)
        self._radio_spectrum_filter_limit_avg = QRadioButton("avg")
        self._radio_spectrum_filter_limit_stdev = QRadioButton("stdev")
        self._radio_spectrum_filter_limit_2stdev = QRadioButton("2x stdev")
        self._radio_spectrum_filter_limit_3stdev = QRadioButton("3x stdev")
        self._radio_spectrum_filter_limit_disabled = QRadioButton("disabled")
        self._radio_spectrum_filter_limit_disabled.setChecked(True)
        self._radio_spectrum_filter_group_limit = QButtonGroup()
        self._radio_spectrum_filter_group_limit.addButton(self._radio_spectrum_filter_limit_avg)
        self._radio_spectrum_filter_group_limit.addButton(self._radio_spectrum_filter_limit_stdev)
        self._radio_spectrum_filter_group_limit.addButton(self._radio_spectrum_filter_limit_2stdev)
        self._radio_spectrum_filter_group_limit.addButton(self._radio_spectrum_filter_limit_3stdev)
        self._radio_spectrum_filter_group_limit.addButton(self._radio_spectrum_filter_limit_disabled)
        spectrum_layout.addWidget(self._radio_spectrum_filter_limit_avg)
        spectrum_layout.addWidget(self._radio_spectrum_filter_limit_stdev)
        spectrum_layout.addWidget(self._radio_spectrum_filter_limit_2stdev)
        spectrum_layout.addWidget(self._radio_spectrum_filter_limit_3stdev)
        spectrum_layout.addWidget(self._radio_spectrum_filter_limit_disabled)
        self._graph_type_stacked_layout.addWidget(self._spectrum_control_widget)

        draw_parameters_layout.addLayout(self._graph_type_stacked_layout)

        self._button_draw = QPushButton("display")
        self._button_draw.clicked.connect(self.button_action_draw)
        draw_parameters_layout.addWidget(self._button_draw)

        main_layout.addLayout(draw_parameters_layout)

        self.setLayout(main_layout)

    def update_listings(self, filter_by: str):
        self._listings_stacked_layout.currentWidget().update_listing(filter_by)

    def change_listing(self, button):
        button_to_id = {
            "Folder": 0,
            "Database": 1
        }
        self._listings_stacked_layout.setCurrentIndex(button_to_id[button.text()])
        self._listings_stacked_layout.currentWidget().update_listing(self._input_search.text())

    def change_graph_type(self, button):
        button_to_id = {
            "oscillogram": 0,
            "spectrum": 1
        }
        self._graph_type_stacked_layout.setCurrentIndex(button_to_id[button.text()])

    def button_action_draw(self):
        params = self.gather_params()
        plot(params)

    def gather_params(self):
        return {
            "graph_type": self._radio_button_graph_type.checkedButton().text(),
            "start": self._oscillogram_control_start_input.text(),
            "end": self._oscillogram_control_end_input.text(),
            "p": self._checkbox_dimension_p.isChecked(),
            "vx": self._checkbox_dimension_vx.isChecked(),
            "vy": self._checkbox_dimension_vy.isChecked(),
            "vz": self._checkbox_dimension_vz.isChecked(),
            "spectrum_delimiter": self._radio_spectrum_filter_group_limit.checkedButton().text(),
            "location": self._radio_button_group.checkedButton().text(),
            "label": self._listings_stacked_layout.currentWidget().currentItem().text()
            if self._listings_stacked_layout.currentWidget().selectedItems() else None
        }


from PyQt5.Qt import QApplication
import sys


def main():
    app = QApplication(sys.argv)
    window = WorkWindow()
    window.show()
    app.exec_()


if __name__ == "__main__":
    main()
