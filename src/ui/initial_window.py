import time

from PyQt5.Qt import QApplication
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QPushButton, QLabel, QProgressBar
from PyQt5.QtCore import pyqtSignal, QThread
import random

from .configuration_window import AppConfigurationWidget


class RepeatThread(QThread):
    change_value = pyqtSignal(int)
    loading_finished = pyqtSignal()

    def run(self):
        duration = random.choice([2, 3, 4, 4, 4, 4, 5, 5, 6, 7, 8])
        sleep_duration = 0.05
        steps_count = int(round(duration / sleep_duration))
        for i in range(steps_count):
            time.sleep(sleep_duration)
            progress = int(round((i + 1) * 1000 / steps_count))
            self.change_value.emit(progress)
        self.loading_finished.emit()


class InitialAppScreenWidget(QWidget):
    _progress_thread = None
    _child_progress_bar: QProgressBar = None
    _child_progress_label: QLabel = None
    _button_run: QPushButton = None
    _button_configure: QPushButton = None
    _button_exit: QPushButton = None

    _window_configuration: QWidget = None
    _window_work: QWidget = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.init_components()

    def init_components(self):
        self.setGeometry(550, 330, 250, 125)
        self.setContentsMargins(0, 0, 0, 0)

        progress_bar = QProgressBar()
        progress_label = QLabel("Loading...")
        button_run = QPushButton("run")
        button_configure = QPushButton("configure")
        button_exit = QPushButton("exit")

        self._child_progress_bar = progress_bar
        self._child_progress_label = progress_label
        self._button_run = button_run
        self._button_configure = button_configure
        self._button_exit = button_exit

        layout = QVBoxLayout()
        layout.addWidget(button_run)
        layout.addWidget(button_configure)
        layout.addWidget(button_exit)
        layout.addWidget(progress_label)
        layout.addWidget(progress_bar)
        button_run.setGeometry(0, 0, 150, 100)
        progress_bar.setMaximum(1000)
        progress_bar.setGeometry(0, 0, 125, 50)
        self.setLayout(layout)
        progress_label.setVisible(False)
        button_run.setVisible(True)
        button_run.clicked.connect(self.button_action_run)
        button_configure.clicked.connect(self.button_action_configure)
        button_exit.clicked.connect(self.button_action_exit)

    def set_progress_value(self, val):
        self._child_progress_bar.setValue(val)

    def button_action_run(self):
        self._button_run.setDisabled(True)
        self._button_configure.setDisabled(True)
        self._child_progress_label.show()
        self._progress_thread = RepeatThread()
        self._progress_thread.change_value.connect(self.set_progress_value)
        self._progress_thread.start()

    def button_action_configure(self):
        self.setDisabled(True)
        self._window_configuration = AppConfigurationWidget(finish_callback=self.configuration_callback)
        self._window_configuration.show()

    def configuration_callback(self):
        self._window_configuration.close()
        self._window_configuration = None
        self.setDisabled(False)

    @staticmethod
    def button_action_exit():
        QApplication.quit()
