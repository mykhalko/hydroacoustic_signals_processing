from typing import Dict

from PyQt5.QtWidgets import QWidget, QVBoxLayout, QPushButton, QLabel, QProgressBar, QLineEdit, QHBoxLayout, QListWidget

from application_configuration import configuration, save_configuration, load_configuration, available_configurations


class ConfSelectionWidget(QWidget):
    _list_confs: QListWidget = None
    _button_select: QPushButton = None
    _button_cancel: QPushButton = None

    def __init__(self, *args, **kwargs):
        self._click_callback = kwargs.pop("click_callback")
        super().__init__(*args, **kwargs)
        self.init_components()
        self.show()

    def init_components(self):

        main_layout = QVBoxLayout()

        self._list_confs = QListWidget()
        self._list_confs.addItems(available_configurations())

        actions_layout = QHBoxLayout()
        self._button_select = QPushButton("select")
        self._button_cancel = QPushButton("cancel")
        self._button_select.setDisabled(True)
        actions_layout.addWidget(self._button_select)
        actions_layout.addWidget(self._button_cancel)

        main_layout.addWidget(self._list_confs)
        main_layout.addLayout(actions_layout)
        self.setLayout(main_layout)

        self._list_confs.itemClicked.connect(self.enable_select_button)
        self._button_select.clicked.connect(self.select)
        self._button_cancel.clicked.connect(self.cancel)

    def enable_select_button(self):
        self._button_select.setDisabled(False)

    def get_selected_item(self):
        if self._list_confs.selectedItems():
            return self._list_confs.currentItem().text()
        return None

    def select(self):
        return self._click_callback(self.get_selected_item())

    def cancel(self):
        return self._click_callback(None)


class AppConfigurationWidget(QWidget):
    _input_hz_limit: QLineEdit = None
    _input_signals_dir: QLineEdit = None
    _input_data_filename_template: QLineEdit = None
    _input_data_prefix_format: QLineEdit = None
    _input_data_prefix: QLineEdit = None
    _input_data_prefix_length: QLineEdit = None
    _input_data_format: QLineEdit = None
    _input_db_uri: QLineEdit = None
    _input_sampling_frequency: QLineEdit = None
    _conf_inputs: list = None

    _input_conf_name: QLineEdit = None

    _label_hz_limit: QLabel = None
    _label_signals_dir: QLabel = None
    _label_data_filename_template: QLabel = None
    _label_data_prefix_format: QLabel = None
    _label_data_prefix: QLabel = None
    _label_data_prefix_length: QLabel = None
    _label_data_format: QLabel = None
    _label_sampling_frequency: QLabel = None
    _label_db_uri: QLabel = None
    _labels: list = None

    _button_save_configuration: QPushButton = None
    _button_load_configuration = None
    _button_confirm: QPushButton = None
    _button_cancel: QPushButton = None

    def __init__(self, *args, **kwargs):
        self._finish_callback = kwargs.pop("finish_callback")
        super().__init__(*args, **kwargs)
        self.init_components()
        self._conf_selector = None
        self.load_configuration()
        self.show()

    @property
    def conf_to_field_mapping(self) -> Dict[str, QLineEdit]:
        return {
            "HZ_LIMIT": self._input_hz_limit,
            "SIGNALS_DIR": self._input_signals_dir,
            "DATA_FILENAME_TEMPLATE": self._input_data_filename_template,
            "DATA_PREFIX": self._input_data_prefix,
            "DATA_PREFIX_FORMAT": self._input_data_prefix_format,
            "DATA_PREFIX_LENGTH": self._input_data_prefix_length,
            "DATA_FORMAT": self._input_data_format,
            "SAMPLING_FREQUENCY": self._input_sampling_frequency,
            "DB_URI": self._input_db_uri
        }

    @property
    def field_to_conf_mapping(self):
        return {v: k for k, v in self.conf_to_field_mapping.items()}

    def load_configuration(self):
        name = str(self._input_conf_name.text())
        if name:
            load_configuration(name)
        for conf_key, field in self.conf_to_field_mapping.items():
            field.setText(str(getattr(configuration, conf_key)))

    def save_configuration(self):
        conf_name = self._input_conf_name.text()
        configuration.set_values({key: field.text() for key, field in self.conf_to_field_mapping.items()})
        save_configuration(conf_name)

    def select_conf_callback(self, name: str):
        if name:
            self._input_conf_name.setText(name)
            self.load_configuration()
        self._conf_selector.close()
        self.setDisabled(False)

    def select_conf(self):
        self._conf_selector = ConfSelectionWidget(click_callback=self.select_conf_callback)
        self.setDisabled(True)
        self._conf_selector.show()

    def init_components(self):
        self._input_hz_limit = QLineEdit()
        self._input_signals_dir = QLineEdit()
        self._input_data_filename_template = QLineEdit()
        self._input_data_prefix = QLineEdit()
        self._input_data_prefix_format = QLineEdit()
        self._input_data_prefix_length = QLineEdit()
        self._input_data_format = QLineEdit()
        self._input_sampling_frequency = QLineEdit()
        self._input_db_uri = QLineEdit()
        self._conf_inputs = [
            self._input_hz_limit,
            self._input_signals_dir,
            self._input_data_filename_template,
            self._input_data_prefix,
            self._input_data_prefix_format,
            self._input_data_prefix_length,
            self._input_data_format,
            self._input_sampling_frequency,
            self._input_db_uri
        ]

        self._label_hz_limit = QLabel("HZ limit")
        self._label_signals_dir = QLabel("Signals folder")
        self._label_data_filename_template = QLabel("Filename tempalte")
        self._label_data_prefix = QLabel("Data prefix")
        self._label_data_prefix_format = QLabel("Data prefix format")
        self._label_data_prefix_length = QLabel("Data prefix length")
        self._label_data_format = QLabel("Data format")
        self._label_sampling_frequency = QLabel("Sampling frequency")
        self._label_db_uri = QLabel("DB uri")
        self._labels = [
            self._label_hz_limit,
            self._label_signals_dir,
            self._label_data_filename_template,
            self._label_data_prefix,
            self._label_data_prefix_format,
            self._label_data_prefix_length,
            self._label_data_format,
            self._label_sampling_frequency,
            self._label_db_uri
        ]

        main_layout = QVBoxLayout()

        input_layout = QHBoxLayout()
        labels_layout = QVBoxLayout()
        fields_layout = QVBoxLayout()
        input_layout.addLayout(labels_layout)
        input_layout.addLayout(fields_layout)
        for input_field, label in zip(self._conf_inputs, self._labels):
            labels_layout.addWidget(label)
            fields_layout.addWidget(input_field)
        main_layout.addLayout(input_layout)

        self._input_conf_name = QLineEdit()
        self._button_save_configuration = QPushButton("save as")
        self._button_load_configuration = QPushButton("load")
        self._button_confirm = QPushButton("confirm")
        self._button_cancel = QPushButton("cancel")

        saving_layout = QHBoxLayout()
        finish_conf_actions_layout = QHBoxLayout()
        saving_layout.addWidget(self._button_save_configuration)
        saving_layout.addWidget(self._input_conf_name)
        finish_conf_actions_layout.addWidget(self._button_load_configuration)
        finish_conf_actions_layout.addWidget(self._button_confirm)
        finish_conf_actions_layout.addWidget(self._button_cancel)

        main_layout.addLayout(saving_layout)
        main_layout.addLayout(finish_conf_actions_layout)
        self.setLayout(main_layout)

        self._button_load_configuration.clicked.connect(self.load_configuration)
        self._button_save_configuration.clicked.connect(self.save_configuration)
        self._button_load_configuration.clicked.connect(self.select_conf)
        self._button_cancel.clicked.connect(self._finish_callback)
        self._button_confirm.clicked.connect(self._finish_callback)
