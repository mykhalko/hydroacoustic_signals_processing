from sqlalchemy.engine import create_engine as _create_engine

from application_configuration import configuration


def create_engine():
    return _create_engine(configuration.DB_URI)
