from contextlib import contextmanager

from sqlalchemy.orm import Session

from .engine import create_engine


@contextmanager
def create_session():
    session = Session(create_engine())
    yield session
    session.close()
