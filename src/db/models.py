from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, VARCHAR, BigInteger, Index, ForeignKey, JSON
from sqlalchemy.orm import relationship


Base = declarative_base()


class Signal(Base):
    __tablename__ = "signals"
    __table_args__ = (Index("lbl_id_index", "meta_id", "id"), )

    id = Column(BigInteger(), primary_key=True)
    p = Column(Integer(), nullable=False)
    vx = Column(Integer(), nullable=True)
    vy = Column(Integer(), nullable=True)
    vz = Column(Integer(), nullable=True)
    meta_id = Column(Integer(), ForeignKey("metainfo.id", ondelete="CASCADE"), nullable=False)
    meta = relationship("Metainfo", lazy=True)


class Metainfo(Base):
    __tablename__ = "metainfo"

    id = Column(Integer(), primary_key=True)
    label = Column(VARCHAR(50), nullable=False, index=True)
    obj_info = Column(JSON(), nullable=True)
    signals = relationship("Signal", lazy=True, cascade="delete")
