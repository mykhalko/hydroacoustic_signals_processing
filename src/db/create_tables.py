from legacy_db.models import Base

from legacy_db.engine import create_engine


def main():
    Base.metadata.create_all(create_engine())


if __name__ == "__main__":
    main()
