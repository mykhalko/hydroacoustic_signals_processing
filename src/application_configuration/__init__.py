import json

from typing import List
from constants import BASE_PATH
from decorators import singleton


CONF_PATH = BASE_PATH / ".conf"


@singleton
class Configuration:
    HZ_LIMIT: int = None
    SIGNALS_DIR: str = None
    DATA_FILENAME_TEMPLATE: str = None
    DATA_PREFIX: str = None
    DATA_PREFIX_FORMAT: str = None
    DATA_PREFIX_LENGTH: int = None
    DATA_FORMAT: str = None
    SAMPLING_FREQUENCY: int = None
    DB_URI: str = None

    def set_values(self, values: dict):
        for key, value in values.items():
            setattr(self, key, value)

    @property
    def dict(self):
        return {
            "HZ_LIMIT": int(self.HZ_LIMIT),
            "SIGNALS_DIR": self.SIGNALS_DIR,
            "DATA_FILENAME_TEMPLATE": self.DATA_FILENAME_TEMPLATE,
            "DATA_PREFIX": eval(self.DATA_PREFIX),
            "DATA_PREFIX_FORMAT": self.DATA_PREFIX_FORMAT,
            "DATA_PREFIX_LENGTH": int(self.DATA_PREFIX_LENGTH),
            "DATA_FORMAT": self.DATA_FORMAT,
            "SAMPLING_FREQUENCY": int(self.SAMPLING_FREQUENCY),
            "DB_URI": self.DB_URI,
        }


def load_configuration(conf_name: str) -> Configuration:
    path = CONF_PATH / f"{conf_name}.json"
    with open(path) as f:
        configuration.set_values(json.loads(f.read()))
    return configuration


def save_configuration(conf_name: str) -> None:
    path = CONF_PATH / f"{conf_name}.json"
    with open(path, "w") as f:
        f.write(json.dumps(configuration.dict, indent=4))


def available_configurations() -> List[str]:
    return [conf.stem for conf in CONF_PATH.iterdir() if conf.suffix == ".json"]


configuration = Configuration()
load_configuration("default")
