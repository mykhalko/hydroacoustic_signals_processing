import os
import struct
import sys
import typing as t
import json
import pandas
from collections import namedtuple
from matplotlib import pyplot

from pathlib import Path

import constants
from application_configuration import configuration



HydroRecord = namedtuple("HydroRecord", ("vx", "vy", "vz", "p"))
dataframe_columns = ("vx", "vy", "vz", "p")


def load(file_path) -> pandas.DataFrame:
    with open(file_path, "rb") as file:
        raw_bytes_data = file.read()
        prefix_bytes = raw_bytes_data[:configuration.DATA_PREFIX_LENGTH]
        data_bytes = raw_bytes_data[configuration.DATA_PREFIX_LENGTH:]
        prefix = struct.unpack(configuration.DATA_PREFIX_FORMAT, prefix_bytes)
        assert prefix == configuration.DATA_PREFIX
        return pandas.DataFrame(struct.iter_unpack(configuration.DATA_FORMAT, data_bytes), columns=dataframe_columns)


def load_by_name(name):
    file_path = Path(constants.BASE_PATH).parent / configuration.SIGNALS_DIR / name
    return load(file_path)


def load_by_id(data_id: int) -> pandas.DataFrame:
    data_filename = configuration.DATA_FILENAME_TEMPLATE.format(data_id)
    file_path = Path(configuration.SIGNALS_DIR) / data_filename
    return load(file_path)
