from multiprocessing import Process

import constants
from application_configuration import configuration


def plot(kwargs):
    kwargs["conf"] = configuration.dict
    process = Process(target=_plot, kwargs=kwargs)
    process.start()


def _plot(**kwargs):
    import numpy as np
    import pandas as pd
    from core import volume_data_utils
    from application_configuration import configuration
    from matplotlib import pyplot
    from core.spectrum import sampling
    configuration.set_values(kwargs["conf"])
    if kwargs["location"].lower() == "folder":
        df = volume_data_utils.load_by_name(kwargs["label"])
    else:
        df = pd.DataFrame()

    if kwargs["graph_type"] == "oscillogram":
        start = float(kwargs["start"] or 0)
        end = float(kwargs["end"] or 10)
        if not (0 <= start <= 10):
            start = 0
        if not (0 <= end <= 10):
            end = 10
        if start >= end:
            start, end = 0, 10
        index_start = int(start * len(df) / 10)
        index_end = int(end * len(df) / 10)
        df = df.iloc[index_start:index_end]
        if kwargs["p"]:
            pyplot.plot(range(index_start, index_end), df.p, color="k")
        if kwargs["vx"]:
            pyplot.plot(range(index_start, index_end), df.vx, color="r")
        if kwargs["vy"]:
            pyplot.plot(range(index_start, index_end), df.vy, color="g")
        if kwargs["vz"]:
            pyplot.plot(range(index_start, index_end), df.vz, color="b")
        pyplot.show()
    else:
        py, vxy, vyy, vzy = None, None, None, None
        colors = ("k", "r", "g", "b")
        if kwargs["p"]:
            py, px, plt = pyplot.magnitude_spectrum(df.p, Fs=configuration.SAMPLING_FREQUENCY)
            py = sampling(np.column_stack((px, py)), configuration.HZ_LIMIT)
        if kwargs["vx"]:
            vxy, vxx, plt = pyplot.magnitude_spectrum(df.vx, Fs=configuration.SAMPLING_FREQUENCY)
            vxy = sampling(np.column_stack((vxx, vxy)), configuration.HZ_LIMIT)
        if kwargs["vy"]:
            vyy, vyx, plt = pyplot.magnitude_spectrum(df.vy, Fs=configuration.SAMPLING_FREQUENCY)
            vyy = sampling(np.column_stack((vyx, vyy)), configuration.HZ_LIMIT)
        if kwargs["vz"]:
            vzy, vzx, plt = pyplot.magnitude_spectrum(df.vz, Fs=configuration.SAMPLING_FREQUENCY)
            vzy = sampling(np.column_stack((vzx, vzy)), configuration.HZ_LIMIT)
        pyplot.close()
        for y, color in zip((py, vxy, vyy, vzy), colors):
            if y is not None:
                pyplot.bar(range(configuration.HZ_LIMIT), list(y), color=color)
        pyplot.show()
