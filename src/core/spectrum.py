import pylab
import pandas as pd
import numpy as np
from types import SimpleNamespace

import constants


def spectrum(values):
    # count = len(values)
    # return (abs(pylab.fft(values) / (2048)))[range(256)]
    length = len(values)
    y = pylab.fft(values) / length
    y = y[range(constants.HZ_LIMIT)]
    return abs(y)


def plotSpectrum(y, Fs):
    n = len(y)  # length of the signal
    k = np.arange(n)
    T = n / Fs
    frq = k / T  # two sides frequency range
    frq = frq[range(n // 2)]  # one side frequency range

    Y = pylab.fft(y) / n  # fft computing and normalization
    Y = Y[range(n // 2)]

    pylab.plot(frq[range(256)], abs(Y)[range(256)], 'r')  # plotting the spectrum
    pylab.xlabel('Freq (Hz)')
    pylab.ylabel('|Y(freq)|')


def plot_spectrum(ds: pd.DataFrame, bar_view=False):
    vx_spectrum = spectrum(ds.vx)
    vy_spectrum = spectrum(ds.vy)
    vz_spectrum = spectrum(ds.vz)
    p_spectrum = spectrum(ds.p)

    graph = pylab.bar if bar_view else pylab.plot
    pylab.subplot(4, 1, 1)
    graph(range(constants.HZ_LIMIT), vx_spectrum, color="r")
    pylab.subplot(4, 1, 2)
    graph(range(constants.HZ_LIMIT), vy_spectrum, color="g")
    pylab.subplot(4, 1, 3)
    graph(range(constants.HZ_LIMIT), vz_spectrum, color="b")
    pylab.subplot(4, 1, 4)
    graph(range(constants.HZ_LIMIT), p_spectrum, color="k")
    pylab.show()


def filter_by_average(spectrum):
    avg = sum(spectrum) / len(spectrum)
    return [value for value in spectrum if value > avg]


def filter_first(spectrum: np.ndarray):
    arr = np.array(spectrum)
    arr[0] = 0
    return arr


def filter_start(spectrum: np.ndarray):
    index = 0
    while spectrum[index] > 1:
        spectrum[index] = 0
        index = index + 1
    return spectrum


def filter_by_std(spectrum, mult):
    std = np.std(spectrum)
    filtered_spectrum = np.empty(len(spectrum))
    for i, v in enumerate(spectrum):
        filtered_spectrum[i] = v if v > mult * std else 0
    return filtered_spectrum


def filter_neighbours(spectrum):
    size = len(spectrum)
    predicted_top = set()
    top = set()
    for i, value in enumerate(spectrum):
        left_value = spectrum[i - 1] if i > 0 else 0
        right_value = spectrum[i + 1] if i < size - 1 else 0
        if value >= left_value and value >= right_value:
            if value > left_value and value > right_value:
                top.add(i)
                predicted_top.clear()
            elif value == left_value and value == right_value:
                predicted_top.add(i)
            elif value > right_value:
                predicted_top.add(i)
                top.update(predicted_top)
                predicted_top.clear()
            elif value > left_value:
                predicted_top.clear()
                predicted_top.add(i)
        else:
            predicted_top.clear()
    for i in top:
        index = i - 1
        comp_value = spectrum[i]
        while index >= 0 and spectrum[index] < comp_value:
            comp_value = spectrum[index]
            spectrum[index] = 0
            index = index - 1

        comp_value = spectrum[i]
        index = i + 1
        while index < size and spectrum[index] < comp_value:
            comp_value = spectrum[index]
            spectrum[index] = 0
            index = index + 1

    return spectrum


def sampling(values, end=256):
    index = 0
    for current_limit in range(1, end + 1):
        current_limit = current_limit - 0.5
        gap_sample = 0
        while values[index][0] < current_limit:
            if values[index][1] > gap_sample:
                gap_sample = values[index][1]
            index = index + 1
        yield gap_sample


def discard_noises_by_avg(values):
    avg = sum(values) / len(values)
    return [x if x >= avg else 0 for x in values]
