from abc import ABCMeta
from abc import abstractmethod
from typing import List
from pathlib import Path

from application_configuration import configuration
from constants import BASE_PATH

from db.session import create_session
from db.models import Metainfo


class AbstractListing(metaclass=ABCMeta):
    @abstractmethod
    def list_items(self, filter_by: str = "") -> List[str]:
        pass


class VolumeListing(AbstractListing):
    def list_items(self, filter_by: str = "") -> List[str]:
        return [file.name for file in self.signals_dir.iterdir() if filter_by.lower() in file.name.lower()]

    @property
    def signals_dir(self) -> Path:
        return BASE_PATH.parent / configuration.SIGNALS_DIR


class DatabaseListing(AbstractListing):
    def list_items(self, filter_by: str = "") -> List[str]:
        filter_by = f"%{filter_by}%"
        with create_session() as session:
            return [row[0] for row in session.query(Metainfo.label).filter(Metainfo.label.ilike(filter_by)).all()]
