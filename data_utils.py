import os
import struct
import sys
import typing as t
import json
import pandas
from collections import namedtuple
from matplotlib import pyplot

import constants
from legacy_db.models import Pressure
from legacy_db.session import create_session


HydroRecord = namedtuple("HydroRecord", ("vx", "vy", "vz", "p"))
dataframe_columns = ("vx", "vy", "vz", "p")


def load(file_path) -> pandas.DataFrame:
    with open(file_path, "rb") as file:
        raw_bytes_data = file.read()
        prefix_bytes = raw_bytes_data[:constants.DATA_PREFIX_LENGTH]
        data_bytes = raw_bytes_data[constants.DATA_PREFIX_LENGTH:]
        prefix = struct.unpack(constants.DATA_PREFIX_FORMAT, prefix_bytes)
        assert prefix == constants.DATA_PREFIX
        return pandas.DataFrame(struct.iter_unpack(constants.DATA_FORMAT, data_bytes), columns=dataframe_columns)


def load_by_id(data_id: int) -> pandas.DataFrame:
    data_filename = constants.DATA_FILENAME_TEMPLATE.format(data_id)
    file_path = os.path.join(constants.SIGNALS_DIR, data_filename)
    return load(file_path)


def draw(data: pandas.DataFrame, second: int, seconds_count: float = 1):
    start = (second - 1) * constants.SAMPLING_FREQUENCY
    end = start + int(constants.SAMPLING_FREQUENCY * seconds_count)
    display_data = data.iloc[start:end]
    abscissa_label = "t (1/2048s)"
    print("pyplot start")
    pyplot.plot(*(display_data.index, display_data.vx), label="vx", color="r")
    pyplot.plot(*(display_data.index, display_data.vy), label="vy", color="g")
    pyplot.plot(*(display_data.index, display_data.vz), label="vz", color="b")
    pyplot.plot(*(display_data.index, display_data.p), label="p", color="k")
    pyplot.show()


def import_to_database():
    folder_items = os.listdir(constants.SIGNALS_DIR)
    items_count = len(folder_items)
    print("Loading files...")
    last_percent = 0
    for progress, folder_item in enumerate(folder_items, start=1):
        path = os.path.join(constants.SIGNALS_DIR, folder_item)
        if os.path.isfile(path):
            with create_session() as session:
                session.bulk_save_objects(Pressure(vx=i.vx, vy=i.vy, vz=i.vz, p=i.p) for i in load(path))
                session.commit()
        percent = (progress * 100) // items_count
        if percent != last_percent:
            print(f"{percent}% completed")
            last_percent = percent


def output_unique(inform_progress_out=sys.stdout, store_to="values.json"):
    data = {
        "vx": set(),
        "vy": set(),
        "vz": set(),
        "p": set()
    }
    folder_items = os.listdir(constants.SIGNALS_DIR)
    items_count = len(folder_items)
    print("Processing files...")
    last_percent = 0
    for progress, folder_item in enumerate(folder_items, start=1):
        path = os.path.join(constants.SIGNALS_DIR, folder_item)
        if os.path.isfile(path):
            ds = list(load(path))
            for key, storage in data.items():
                storage.update(map(lambda i: getattr(i, key), ds))
        percent = (progress * 100) // items_count
        if percent != last_percent:
            print(f"{percent}% completed", file=inform_progress_out)
            last_percent = percent
    print("Sorting values...", file=inform_progress_out)
    for key, value in data.values():
        data[key] = list(value)
        data[key].sort()
    print(f"Writing values to <{store_to}>...", file=inform_progress_out)
    with open(store_to, "w") as file:
        file.write(json.dumps(data))
    print("Process finished", file=inform_progress_out)
