import sys
import numpy as np
from matplotlib import pyplot
from spectrum import filter_start, sampling, discard_noises_by_avg
from data_utils import load_by_id
from datetime import datetime, timedelta


def sampled_spectrum(file_id):
    ds = load_by_id(file_id)
    s_y, s_x, plot = pyplot.magnitude_spectrum(ds.p, Fs=2048)
    pyplot.close()
    s_y = filter_start(s_y)
    sampled_spectrum = list(sampling(np.column_stack((s_x, s_y))))
    filtered_spectrum = discard_noises_by_avg(sampled_spectrum)
    return filtered_spectrum


def main():
    count, filename = sys.argv[1:]
    count = int(count)
    last_percent = 0
    start = datetime.now()
    print("started at {}".format(start.strftime("%H:%M:%S")))
    with open(filename, "w") as f:
        for i in range(count):
            vector = sampled_spectrum(i)
            line = ",".join([str(x) for x in vector]) + "\n"
            f.write(line)
            percent = int((i + 1) * 100 / count)
            if percent != last_percent:
                delta = datetime.now() - start
                minutes, seconds = divmod(delta.seconds, 60)
                hours, minutes = divmod(minutes, 60)
                print(f"{percent}% completed", end="; ")
                print(f"time spent: {hours:02d}:{minutes:02d}:{seconds:02d}", end="; ")
                estimated_seconds_left = round((delta.seconds / percent)) * (100 - percent)
                est_minutes, est_seconds = divmod(estimated_seconds_left, 60)
                est_hours, est_minutes = divmod(est_minutes, 60)
                print(f"time left: {est_hours:02d}:{est_minutes:02d}:{est_seconds:02d}")
                last_percent = percent


if __name__ == "__main__":
    main()
