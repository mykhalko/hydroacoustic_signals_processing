from multiprocessing import Process
from datetime import datetime, timedelta

from process_plotter import plotter


def main():
    prev_time = None
    min_delta = timedelta(seconds=1)
    for i in range(0, 7370, 1):
        # p = Process(target=plotter, args=(0,))
        # p.start()
        # p.join()
        plotter(i)
        if prev_time:
            current_time = datetime.utcnow()
            if current_time - prev_time < min_delta:
                break
        prev_time = datetime.utcnow()


if __name__ == "__main__":
    main()
