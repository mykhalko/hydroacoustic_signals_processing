import pandas as pd


def manual_avg_normalization(df: pd.DataFrame, period: int) -> pd.DataFrame:
    normalized_ds = df.groupby(df.index % period).mean()
    rows_count = len(df.index)
    repeat = rows_count // period
    return pd.concat([normalized_ds] * repeat, ignore_index=True)


def manual_blur_noise_reduction(df: pd.DataFrame, period: int, chunk_size: int):
    rows_count = len(df.index)
    chunked_df = [df.iloc[i:i + chunk_size] for i in range(0, rows_count, chunk_size)]
    processed_df = [df_part.groupby(df_part.index % period).mean() for df_part in chunked_df]
    repeat = chunk_size // period
    return pd.concat([pd.concat([df_part] * repeat, ignore_index=True) for df_part in processed_df], ignore_index=True)
