TOP_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))


create-tables:
	PYTHONPATH=$(TOP_DIR)/src $(TOP_DIR)/venv/bin/python $(TOP_DIR)/src/db/create_tables.py
