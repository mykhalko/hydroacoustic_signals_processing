import os

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
SIGNALS_DIR = os.getenv("SIGNALS_DIR", os.path.join(os.path.abspath(os.path.dirname(BASE_DIR)), "sigdat"))
DATA_FILENAME_TEMPLATE = os.getenv("DATA_FILENAME_TEMPLATE", "signal_{}.dat")
DATA_PREFIX_FORMAT = "hh"
DATA_PREFIX = (0, 0)
DATA_PREFIX_LENGTH = 4
DATA_FORMAT = "hhhh"
SAMPLING_FREQUENCY = os.getenv("SAMPLING_FREQUENCY", 2048)
HZ_LIMIT = os.getenv("HZ_LIMIT", 256)
