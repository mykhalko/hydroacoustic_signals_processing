import os


class Config:
    # database
    DB_DIALECT = os.getenv("DB_DIALECT", "postgres")
    DB_USER = os.getenv("DB_USER", "user")
    DB_PASSWORD = os.getenv("DB_PASSWORD", "password")
    DB_HOST = os.getenv("DB_HOST", "127.0.0.1")
    DB_PORT = os.getenv("DB_PORT", "46465")
    DB_NAME = os.getenv("DB_NAME", "hadb")

    @property
    def db_uri(self) -> str:
        template = "{dialect}://{user}:{password}@{host}:{port}/{name}"
        return template.format(
            dialect=self.DB_DIALECT,
            user=self.DB_USER,
            password=self.DB_PASSWORD,
            host=self.DB_HOST,
            port=self.DB_PORT,
            name=self.DB_NAME
        )
